import { Redirect, Route, Switch } from 'react-router';
import Portfolio from './components/portfolio';
import { UploadPhotosPage } from './components/uploads-photos-page/uploads-photos-page';
import { PhotosPage } from './components/photos-page/PhotosPage';
import { AboutAuthor } from './components/about-author';
import { Conditions } from './components/conditions';
import { EnterToAdmin } from './components/admin-auth/EnterToAdmin';
import {Contacts} from "./components/contacts/Contacts";
import React from 'react';


export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <Switch>
        <Route path='/portfolio' exact render={() => <Portfolio />} />
        <Route
          path='/album/:id'
          render={({ match, history }) => {
            const { id } = match.params;
            return (
              <PhotosPage albumId={id} history={history} />
            );
          }}
        />
        <Route path='/author' component={AboutAuthor} />
        <Route path='/conditions' component={Conditions} />
        <Route path='/upload' component={UploadPhotosPage} />
          <Route path='/contacts' component={Contacts} />
        <Redirect to='/portfolio' />
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path='/portfolio' exact render={() => <Portfolio />} />
      <Route
        path='/album/:id'
        render={({ match, history }) => {
          const { id } = match.params;
          return <PhotosPage albumId={id} history={history} />;
        }}
      />
      <Route path='/author' component={AboutAuthor} />
      <Route path='/conditions' component={Conditions} />
      <Route path='/auth' component={EnterToAdmin} />
      <Route path='/contacts' component={Contacts} />
      <Redirect to='/portfolio' />
    </Switch>
  );
};
