import {createContext} from 'react'

export const AlbumsContext = createContext({
    albums: null,
})

