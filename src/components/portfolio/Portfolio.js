import React, {useCallback, useContext} from 'react';
import {Preloader} from '../preloader';
import {AlbumList} from '../album-list';
import styles from './Portfolio.module.css';
import {AuthContext} from '../../context/AuthContext'
import {AlbumsContext} from '../../context/AlbumsContext'
import {withRouter} from "react-router";
import {apiService} from "../../services/apiService";
import {useHttp} from "../../hooks/http-hook";

const Portfolio = ({history}) => {
    const albumsContext = useContext(AlbumsContext);
    const auth = useContext(AuthContext);
    const {getAllAlbums} = apiService;
    const {data} = useHttp(useCallback(() => getAllAlbums(), []));
    albumsContext.albums = data;
    if (!albumsContext.albums) return (<div className={styles.preloader}><Preloader/></div>);
    console.log(data)
    return (

        <div className={styles.portfolio}>
            <div className={styles.header}>
                <div className={styles.title}>Портфолио</div>
                {auth.isAuthenticated && <div className={styles.addAlbumButton} onClick={() => {
                    history.push('/upload')

                }}>
                    <div className={styles.addAlbum}>Добавить серию <span className={styles.plus}>+</span></div>
                </div>
                }
            </div>

            <AlbumList
                albums={albumsContext.albums}
                onItemSelected={id => history.push(`/album/${id}`)}
            />
        </div>


    );
};

export default withRouter(Portfolio);