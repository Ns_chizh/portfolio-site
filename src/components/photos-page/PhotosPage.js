import React, {useCallback, useContext} from 'react';
import {Preloader} from '../preloader/preloader';
import {PhotoList} from '../photo-list/PhotoList';
import styles from './PhotosPage.module.css';
import {apiService} from '../../services/apiService';
import {useHttp} from "../../hooks/http-hook";
import {AlbumsContext} from "../../context/AlbumsContext";


export function PhotosPage(props) {
    const albumsContext = useContext(AlbumsContext);
    const {getAllPhotos} = apiService;
    const currentId = props.history.location.pathname.split('/')[2];
    const {data, loading, error} = useHttp(
        useCallback(() => getAllPhotos(currentId), [currentId])
    );

    const {albumId, history} = props;
    const {albums} = albumsContext;
    let album = data;
    let isArrowLeft = true;
    let isArrowRight = true;
    let indexOfAlbum = -1;
    let isLoading = loading;
    let errors = error;
    if (isLoading) {
        return (<div className={styles.preloader}><Preloader/></div>);
    }
    ;

    albums.map((item, i) => {
        if (item._id === albumId) {
            indexOfAlbum = i;
        }
        return null;
    });

    const showNextAlbum = () => {
        isLoading = true;

        if (indexOfAlbum === albums.length - 1) {
            return null;
        } else {
            if (indexOfAlbum === 0) {
                isArrowLeft = true;
            }
            indexOfAlbum++;

            if (indexOfAlbum === albums.length - 1) {
                isArrowRight = false;
            }

            history.push(`/album/${albums[indexOfAlbum]._id}`)
            window.location.reload()
        }
    };

    const showPreviousAlbum = () => {
        isLoading = true;

        if (indexOfAlbum === 0) {
            return;
        } else {
            indexOfAlbum--;

            if (indexOfAlbum === albums.length - 2) {
                isArrowRight = true;
            }
            if (indexOfAlbum === 0) {
                isArrowLeft = false;
            }

            history.push(`/album/${albums[indexOfAlbum]._id}`);
            window.location.reload()
        }
    };


    const {name, photos, _id} = album;

    const errorMessage = errors ? <div>Ошибка: {errors.message}</div> : null;
    const loader = isLoading ? <Preloader/> : null;

    return (
        <div className={styles.album}>
            {errorMessage}
            {loader}
            {!isLoading && (
                <div>
                    <div className={styles.header}>
                        {isArrowLeft && indexOfAlbum > 0 && (
                            <div className={styles.doubleLeft} onClick={showPreviousAlbum}><img src="../icons/double-right.png" alt=""/></div>)}
                        {isArrowLeft && indexOfAlbum > 0 && (
                            <div
                                className={styles.arrowLeft}
                                onClick={showPreviousAlbum}
                            >
                                Предыдущий альбом
                            </div>
                        )}
                        <div className={styles.title}>{name}</div>
                        {isArrowRight && indexOfAlbum < albums.length - 1 && (
                            <div className={styles.arrowRight} onClick={showNextAlbum}>
                                Следующий альбом
                            </div>
                        )}
                        {isArrowRight && indexOfAlbum < albums.length - 1 && (
                            <div className={styles.doubleRight} onClick={showNextAlbum}><img src="../icons/double-right.png" alt=""/></div>)}

                    </div>
                    <div className={styles.container}>
                        <PhotoList photos={photos} albumId={_id}/>
                    </div>
                </div>
            )}
        </div>
    );
}

