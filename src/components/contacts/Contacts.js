import styles from "./Contacts.module.css"
import React from "react";

export function Contacts() {
    return (

        <div className={styles.contacts}>
            <div className={styles.link}>
                <div htmlFor="" className={styles.label}>WhatsApp</div>
                <div className={styles.iconLink}><a href="https://wa.me/79059148895" target="_blank"
                                                    rel="noopener noreferrer"><img className={styles.whatsApp}
                                                                                   src="../icons/whatsapp.png"
                                                                                   alt="whatsapp"/></a></div>
            </div>
            <div className={styles.link}>
                <div htmlFor="" className={styles.label}>Instagram</div>
                <div className={styles.iconLink}><a href="https://instagram.com/photocase__?igshid=u1v800y1psjx"
                                                    target="_blank" rel="noopener noreferrer"><img
                    className={styles.instagram} src="../icons/instagram.png" alt="instagram"/></a></div>
            </div>
        </div>
    )
}