import React from "react";
import styles from "./AboutAuthor.module.css"


export function AboutAuthor() {
    return (
        <div className={styles.about}>
            <div className={styles.header}>
                <div className={styles.title}>Об авторе</div>
            </div>
            <div className={styles.container}>
                <div className={styles.content}>
                    <div className={styles.mainText}>
                        <p>
                            Привет, меня зовут Юля, и я буду рада стать твоим фотографом.
                            <br/>
                            <br/>
                            Расскажу
                            немного о себе для представления о том, с кем предстоит работать.
                            <br/>
                            <br/>
                            Мне 20 лет, переехала в Сочи из холодной Сибири 3 года назад. И 2 из них занимаюсь
                            фотоискусством.
                            <br/>
                            <br/>
                        </p>
                    </div>
                    <div className={styles.imageBlock}>
                        <img src="./images/about.JPG" className={styles.img} alt=""/>
                    </div>

                </div>
                <div className={styles.bottomText}>
                    С самого детства мечтала стать фотографом и вот наконец-то доросла до воплощения этой идеи.
                    Фотоаппарат в моих руках появился много лет назад, но «кнопконажиматель», как оказалось, не равно
                    фотограф.
                    Поэтому последние два года я отдаю все свои силы на развитие себя в этом направлении.
                </div>
            </div>
        </div>
    )
}