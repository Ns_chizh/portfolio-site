import React from 'react';
import { useFileHandlers } from '../../hooks/useFileHandlers';
import { PhotosView } from './photos-view';

import style from './uploads-photos-page.module.css';
import { Form } from './form';
import { Preloader } from '../preloader';
import { CoverView } from './cover-view/cover-view';

export const UploadPhotosPage = () => {
  const {
    cover,
    photos,
    albumTitle,
    loading,
    uploaded,
    error,
    onChangeCover,
    onChangePhotos,
    onSubmit,
    onChangeInput,
    isValidForm,
  } = useFileHandlers();

  const loader = loading ? <Preloader /> : null;
  const photosView = !loading ? <PhotosView photos={photos} /> : null;

  return (
    <div className={style.container}>
      <h1 className={style.main_title}>Загрузка нового альбома</h1>
      <div className={style.content}>
        <div className={style.cover_wrap}>
          <CoverView cover={cover} loading={loading} />
        </div>
        <div className={style.form}>
          <Form
            onChangeCover={onChangeCover}
            onChangePhotos={onChangePhotos}
            onSubmit={onSubmit}
            onChangeInput={onChangeInput}
            value={albumTitle}
            disabled={!isValidForm}
            uploaded={uploaded}
            error={error}
          />
        </div>
      </div>

      <div className={style.content_photos}>
        {loader}
        {photosView}
      </div>
    </div>
  );
};
