import React from 'react';
import { InputBtn } from '../input-btn';
import { Input } from '../input';

import style from './form.module.css';
import icon from './success-arrow.svg';

export const Form = (props) => {
  const { onChangePhotos, onChangeCover, error, uploaded } = props;

  const succesMessageBlock =
    !error && uploaded ? <SuccessMessageBlock /> : null;
  const errorBlock = error && !uploaded ? <ErrorBlock /> : null;

  return (
    <>
      <Input {...props} />
      <div className={style.buttons}>
        <InputBtn {...props} onChange={onChangeCover}>
          добавить обложку
        </InputBtn>
        <InputBtn {...props} onChange={onChangePhotos} multiple={true}>
          добавить фотографии
        </InputBtn>
      </div>

      <button
        className={style.btn}
        onClick={props.onSubmit}
        disabled={props.disabled}
      >
        Загрузить
      </button>
      {succesMessageBlock}
      {errorBlock}
    </>
  );
};

const SuccessMessageBlock = () => {
  return (
    <div className={style.success__block}>
      <img className={style.icon} src={icon} alt=""/>
      <span className={style.success__text}>Фотографии загружены успешно</span>
    </div>
  );
};

const ErrorBlock = () => {
  return (
    <div className={style.error_block}>
      <span className={style.error__text}>
        Что-то пошло не так, попробуйте ещё раз
      </span>
    </div>
  );
};
