import React from 'react';
import style from './input-btn.module.css';

export const InputBtn = ({ children, onChange, multiple = false }) => {
  const inputRef = React.useRef();

  return (
    <>
      <input
        className={style.input_btn}
        type='file'
        multiple={multiple}
        onChange={onChange}
        ref={inputRef}
      />
      <button
        className={`${style.btn} ${style.btn_small}`}
        onClick={() => inputRef.current.click()}
      >
        {children}
      </button>
    </>
  );
};
