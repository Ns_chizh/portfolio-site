import React from 'react';

import style from './photos-view.module.css';

export const PhotosView = ({ photos }) => {
  return (
    <>
      {photos &&
        photos.map(({ id, src, name }) => {
          return (
            <div className={style.container} key={id}>
              <div>
                <img className={style.image} src={src} alt='' />
              </div>
              <div className={style.title}>{name}</div>
            </div>
          );
        })}
    </>
  );
};
