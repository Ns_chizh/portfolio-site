import React from 'react';
import style from './cover-view.module.css';
import icon from '../cover.svg';
import { Preloader } from '../../preloader';

export const CoverView = ({ cover, loading }) => {
  const imgSrc = cover ? cover.src : icon;

  const loader = loading ? <Preloader /> : null;

  const img = !loading ? (
    <img className={style.icon} src={imgSrc} alt='Cover' />
  ) : null;

  return (
    <>
      <h3 className={style.title}>Обложка альбома </h3>
      <div className={style.cover}>
        {img}
        {loader}
      </div>
    </>
  );
};
