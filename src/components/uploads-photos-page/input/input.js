import React  from 'react';
import style from './input.module.css';

export const Input = ({ type, value, onChangeInput, errorMessage }) => {
  const inputType = type || 'text';
  const htmlFor = `${inputType} - ${Math.random()}`;

  return (
    <div className={style.container}>
      <label className={style.label} htmlFor={htmlFor}>
        Название альбома
      </label>
      <input
        className={style.input}
        id={htmlFor}
        type={inputType}
        placeholder='Введите название альбома'
        value={value}
        onChange={onChangeInput}
      />

      <span>{errorMessage}</span>
    </div>
  );
};
