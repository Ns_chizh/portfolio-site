import React, {useContext, useState} from "react";
import styles from "./EnterToAdmin.module.css"
import {apiService} from '../../services/apiService';
import {AuthContext} from '../../context/AuthContext'


export function EnterToAdmin() {

    const auth = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const sendData = () => {
        const {checkAuthAndGetToken} = apiService;
        const data = {
            email,
            password
        };

        checkAuthAndGetToken(data).then(res => {
            if (!res) {
                setEmail('');
                setPassword('');
                alert('Введены неверные данные, попробуйте снова');
                return null;
            }   else {
                auth.login();
            }

        })
    };

    return (
        <div className={styles.container}>
            <div className={styles.form}>
                <div>
                    <input className={styles.inputEmail} type="text" name="username" placeholder="Введите логин"
                           id="email"
                           onChange={(e) => setEmail(e.target.value)} value={email}/>
                </div>
                <div>
                    <input className={styles.inputPassword} type="password" name="password" placeholder="Введите пароль"
                           id="password"
                           onChange={(e) => setPassword(e.target.value)} value={password}/>
                </div>
                <input className={styles.inputSubmit} type="submit" name="submit" value="ВОЙТИ" onClick={sendData}/>
                <br/>
            </div>
        </div>
    )
}

