import React from 'react';
import './preloader.css';

export const Preloader = () => {


  return (
    <div className='preloader'>
      <div className='loadingio-spinner-dual-ring-pslxskjr9is'>
        <div className='ldio-dibp7cezgv'>
          <div></div>
          <div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  );
};
