import React, { useCallback, useContext } from 'react';
import { Sidebar } from '../sidebar';
import { useRoutes } from '../../routes';
import { BrowserRouter } from 'react-router-dom';
import { useMyContext } from '../../hooks/context-hook';
import { AuthContext } from '../../context/AuthContext';
import { AlbumsContext } from '../../context/AlbumsContext';
import { apiService } from '../../services/apiService';
import { useHttp } from '../../hooks/http-hook';
import { Preloader } from '../preloader';
import '../../index.css';
import styles from "./App.module.css";

export function App() {
  const albumsContext = useContext(AlbumsContext);
  const { login, logout, isAuthenticated } = useMyContext();
  const routes = useRoutes(isAuthenticated);

  const { getAllAlbums } = apiService;
  const { data } = useHttp(useCallback(() => getAllAlbums(), []));
  if (!data)  return (<div className={styles.preloader}><Preloader/></div>);

  albumsContext.albums = data;

  return (
    <AuthContext.Provider
      value={{
        login,
        logout,
        isAuthenticated,
      }}
    >
      <BrowserRouter>
        <div className='wrapper'>
          <Sidebar />
          {routes}
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}
