import React, {useContext} from 'react';
import styles from './Sidebar.module.css';
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from '../../context/AuthContext'

export function Sidebar() {
    const history = useHistory();
    const auth = useContext(AuthContext);

    const logoutHandler = event => {
        event.preventDefault();
        auth.logout();
        history.push('/')
    };

    const onclick = ()=>{
        const toggleMenu = document.getElementById('menu');
        toggleMenu.checked = false;
    };

    return (
        <div>
            <div className={styles.sidebar}>
                <label className={styles.header}>Воронкова</label>
                <label className={styles.header}>Юля</label>
                <div className={styles.list}>
                    <div className={styles.portfolio}>
                        <NavLink
                            to='/portfolio'
                            className={styles.link}
                            activeClassName={styles.active}
                        >
                            Портфолио
                        </NavLink>
                    </div>
                    <div className={styles.about}>
                        <NavLink
                            to='/author'
                            className={styles.link}
                            activeClassName={styles.active}
                        >
                            Об авторе
                        </NavLink>
                    </div>
                    <div className={styles.conditions}>
                        <NavLink
                            to='/conditions'
                            className={styles.link}
                            activeClassName={styles.active}
                        >
                            Условия
                        </NavLink>
                    </div>
                </div>
                <div className={styles.icons}>
                    <div><a href="https://wa.me/79059148895" target="_blank"
                            rel="noopener noreferrer"><img className={styles.whatsApp}
                                                           src="../icons/whatsapp.png"
                                                           alt="whatsapp"/></a></div>
                    <div><a href="https://instagram.com/photocase__?igshid=u1v800y1psjx"
                            target="_blank" rel="noopener noreferrer"><img
                        className={styles.instagram} src="../icons/instagram.png" alt="instagram"/></a></div>
                </div>
                {auth.isAuthenticated && <div className={styles.exit} onClick={logoutHandler}>Выход</div>}

            </div>


            <nav className={styles.mobileMenu}>
                <input type="checkbox" name="toggle" id="menu" className={styles.toggleMenu}/>
                <label htmlFor="menu" className={styles.toggleMenu}><i className={`${styles.fa} ${styles.faBars}`}></i>Меню</label>
                <ul className={styles.ul}>
                    <li className={styles.li}>
                        <NavLink
                            to='/portfolio'
                            className={styles.navlink}
                            activeClassName={styles.active}
                            onClick={onclick}
                        >
                            Портфолио
                        </NavLink>
                    </li>
                    <li className={styles.li}>
                        <NavLink
                            to='/author'
                            className={styles.navlink}
                            activeClassName={styles.active}
                            onClick={onclick}
                        >
                            Об авторе
                        </NavLink>
                    </li>
                    <li className={styles.li}>
                        <NavLink
                            to='/conditions'
                            className={styles.navlink}
                            activeClassName={styles.active}
                            onClick={onclick}
                        >
                            Условия
                        </NavLink>
                    </li>
                    <li className={styles.li}>
                        <NavLink
                            to='/contacts'
                            className={styles.navlink}
                            activeClassName={styles.active}
                            onClick={onclick}
                        >
                            Контакты
                        </NavLink>
                    </li>
                </ul>

            </nav>

        </div>
    );
}

