import React, {useContext} from 'react';
import styles from './PhotoList.module.css';
import {AuthContext} from "../../context/AuthContext";
import {apiService} from "../../services/apiService";


export function PhotoList({photos, _id}) {

    const leftColumn = [];
    const centerColumn = [];
    const rightColumn = [];
    const choosePhotos = [];

    const auth = useContext(AuthContext);
    const changePhotoPlaces = (event) => {
        if (!auth.isAuthenticated) return;
        const photo = document.getElementById(`${event.target.id}`);
        if (!photo) return;
        if (photo.style.border === '4px solid red') {
            photo.style.border = 'none';
            choosePhotos.splice(choosePhotos.indexOf(event.target.id), 1);
        } else {
            choosePhotos.push(event.target.id);
            photo.style.border = '4px solid red';
        }

        const button = document.getElementById('button');

        if (choosePhotos.length > 2) {
            const changedPhoto = document.getElementById(`${choosePhotos[0]}`);
            changedPhoto.style.border = 'none';
            choosePhotos.shift();
        } else if (choosePhotos.length === 2) {
            button.disabled = "";
        } else {
            button.disabled = "disabled";
        }
        document.getElementById('button').addEventListener('click',onSubmit);
    };

    for (let i = 0; i < photos.length; i++) {
        if (i >= 0 && i < photos.length / 3) {
            leftColumn.push(photos[i]);
        }
        if (i >= photos.length / 3 && i < photos.length - photos.length / 3) {
            centerColumn.push(photos[i]);
        }
        if (i >= photos.length - photos.length / 3) {
            rightColumn.push(photos[i]);
        }
    }
    const top = photos[photos.length / 3];
    const renderPhoto = (photo) => (
        <div
            key={photo}
            className={`${styles.item} ${photo === top ? styles.top : ''}`}
            onClick={changePhotoPlaces}
        >

            <img src={`/uploads/${photo.path}`} alt={`photo from album ${_id}`} className={`${styles.img} ${auth.isAuthenticated && styles.hover}`}  id={photo.id}/>

        </div>
    );
   const onSubmit = async () => {
       const {changePlaces} = apiService;
       await changePlaces(choosePhotos,'auth/changePhotosPlaces');
       window.location.reload();
   }


    return (
        <div className={styles.container}>
            {
               auth.isAuthenticated && <button disabled="disabled" id="button" className={styles.button} >
                    Поменять местами
                </button>
            }

            <div className={styles.photos}>
                <div className={styles.items}>{leftColumn.map(renderPhoto)}</div>
                <div className={styles.items}>{centerColumn.map(renderPhoto)}</div>
                <div className={styles.items}>{rightColumn.map(renderPhoto)}</div>
            </div>


        </div>
    );
};
