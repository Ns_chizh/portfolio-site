import React, {useContext} from 'react';
import styles from './album-list.module.css';
import {AuthContext} from '../../context/AuthContext'
import {apiService} from "../../services/apiService";

export const AlbumList = ({albums, onItemSelected}) => {
    const auth = useContext(AuthContext);
    const {deleteAlbum} = apiService;
    const choosePhotos = [];

    const changePhotoPlaces = (event) => {
        if (!auth.isAuthenticated) return;

        const photo = document.getElementById(`${event.target.id}`);
        const button = document.getElementById('button');

        if (!photo) return;

        if (!photo.checked ) {
            choosePhotos.splice(choosePhotos.indexOf(event.target.id), 1);
        } else {
            choosePhotos.push(event.target.id);
        }

        if (choosePhotos.length > 2) {
            const changedPhoto = document.getElementById(`${choosePhotos[0]}`);
            changedPhoto.checked = false;
            choosePhotos.shift();
        } else if (choosePhotos.length === 2) {
            button.disabled = "";
        } else {
            button.disabled = "disabled";
        }
        document.getElementById('button').addEventListener('click', onSubmit);
    };
    const onSubmit = async () => {
        const {changePlaces} = apiService;
        await changePlaces(choosePhotos, 'auth/changeAlbumsPlaces');
        window.location.reload();
    }
    const items = albums.map(({_id, cover}) => (
        <div>
            {auth.isAuthenticated &&<div className={styles.blocks}>

                <div className={styles.crossBlock} onClick={async () => {
                let isConfirmed = window.confirm("Удалить альбом?");
                if (isConfirmed) {
                    await deleteAlbum(_id);
                    window.location.reload();
                }
            }}><img
                src="./cross.png" alt="cross" className={styles.cross}/></div>
                <div className={styles.markBlock}><label
                    className={styles.mark}>
                    <input type="checkbox" onChange={changePhotoPlaces}  id={_id}/>
                    <span className={styles.checkmark}></span>
                </label></div>
            </div>}
            <div
                key={_id}
                className={styles.imageBlock}
                onClick={() => onItemSelected(_id)}
            >

                <a className={styles.link} href="javascript:void(0)">
                    <img className={styles.coverImg} src={`/uploads/${cover}`} alt='cover'/>
                </a>
                <br/>
            </div>
        </div>

    ));

    return (<React.Fragment>
        {
            auth.isAuthenticated && <div className={styles.buttonBlock}>
                <button disabled="disabled" id="button" className={styles.button}>
                    Поменять местами
                </button>
            </div>
        }
        <div className={styles.container}>
            {items}</div>
    </React.Fragment>)
};
