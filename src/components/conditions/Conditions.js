import React from "react";
import styles from "./Conditions.module.css"


export function Conditions() {
    const conditionsObjects = [
        {
            id: '1',
            name: 'Портретная съёмка',
            title: 'В студии',
            duration: ['Длительность 1 час:',
                '50+ фотографий в художественной цветокоррекции',
                'и 10 на ваш выбор с детальной ретушью'],
            price: '2000 р.',
            addition: ['Аренда студии оплачивается отдельно.',
                'Помогаю подготовиться к съемке.',
                'Обработка фото в течение недели.',
                'Файлы передаю через Маил облако,',
                'ссылка для скачивания доступна в течение недели.'],
            src: '1_condition.JPG'
        },
        {
            id: '2',
            name: 'Портретная съемка',
            title: 'В городе или на природе',
            duration: ['Длительность 1 час',
                '70+ кадров в художественной цветокоррекции',
                'и 10 на ваш выбор с детальной ретушью'],
            price: '2000 р.',
            addition: ['Помогаю подготовиться к съемке и найти место.',
                'Обработка фото в течение недели.',
                'Файлы передаю через Маил облако,',
                'ссылка для скачивания доступна в течение недели.',
                'Запись по предоплате 1000р.',
                'При отмене сумма не возвращается,',
                'перенести встречу можно один раз.'],
            src: '2_condition.JPG'
        },
        {
            id: '3',
            name: 'Фотоконтент',
            title: 'Фото для вашего профиля / сайта',
            duration: ['За 1 час съемки:',
                '50-70 кадров в художественной цветокоррекции',
                'и 10 на ваш выбор с детальной ретушью.'],
            price: '2000 р.',
            addition: ['Обработка фото в течение недели.',
                'Файлы передаю через Маил облако,',
                'ссылка для скачивания доступна в течение недели.'],
            src: '3_condition.JPG'
        },
        {
            id: '4',
            name: 'Love story',
            title: 'Парная фотосессия',
            duration: ['За 1 час съёмки:',
                '80+ фотографий в художественной цветокоррекции ',
                'и 10 на ваш выбор с детальной ретушью.'],
            price: '2500 р.',
            addition: ['Помогаю подготовиться к съемке и найти место.',
                'Обработка фото в течение недели.',
                'Файлы передаю через Маил облако,',
                'ссылка для скачивания доступна в течение недели.',
                'Запись по предоплате 1000р.',
                'При отмене сумма не возвращается,',
                'перенести встречу можно один раз.'],
            src: '4_condition.jpg'
        },
        {
            id: '5',
            name: 'Семейная/дружеская фотосессия',
            title: 'Для 3 членов семьи и более/для компании друзей',
            duration: ['За 1 час съёмки:',
                '80+ фотографий в художественной цветокоррекции ',
                'и 10 на ваш выбор с детальной ретушью,'],
            price: '3 000 р.',
            addition: ['Помогаю подготовиться к съемке и найти место.',
                'Обработка фото в течение недели.',
                'Файлы передаю через Маил облако,',
                'ссылка для скачивания доступна в течение недели.',
                'Запись по предоплате 1000р.',
                'При отмене сумма не возвращается,',
                'перенести встречу можно один раз.'],
            src: '5_condition.jpg'
        }
    ];

    const arrayOfConditions = conditionsObjects.map((item) => <div key={item.id + item.src} className={styles.blockCondition}>
        <div className={styles.imgBlock}><img className={styles.img} src={`./images/${item.src}`} alt=""/></div>
        <div className={styles.info}>
            <div className={styles.name}>{item.name}</div>
            <div className={styles.title}>{item.title}</div>
            <div className={styles.duration}>{item.duration.map(item => <div>{item}<br/></div>)}</div>
            <div className={styles.price}>{item.price}</div>
            <div className={styles.addition}>{item.addition.map(item => <div>{item}<br/></div>)}</div>
        </div>
    </div>);

    return (
        <div className={styles.conditions}>
            <div className={styles.header}>
                <div className={styles.headerText}>Условия</div>
            </div>
            <div className={styles.content}>
                {arrayOfConditions}
            </div>
        </div>
    )
}


