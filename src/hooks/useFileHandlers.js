import { useState } from 'react';
import { apiService } from '../services/apiService';

export const useFileHandlers = () => {
  const [photos, setPhotos] = useState(null);
  const [albumCover, setAlbumCover] = useState(null);
  const [albumTitle, setAlbumTitle] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [uploaded, setUploaded] = useState(false);

  const isValidForm = () => {
    if (photos && albumCover && albumTitle) return true;
    return false;
  };

  const clearFields = () => {
    setPhotos(null);
    setAlbumCover(null);
    setAlbumTitle('');
  };

  const onChangeCover = (e) => {
    if (e.target.files.length) {
      setUploaded(false);
      setError(false);
      const src = window.URL.createObjectURL(e.target.files[0]);
      const cover = { cover: e.target.files[0], src };
      setAlbumCover(cover);
    }
  };

  const onChangePhotos = (e) => {
    if (e.target.files.length) {
      setUploaded(false);
      setError(false);
      const files = Array.from(e.target.files);
      const photos = files.map((file, i) => {
        const src = window.URL.createObjectURL(file);
        return {
          photo: file,
          src,
          id: file.name + i,
          name: file.name,
        };
      });

      setPhotos(photos);
    }
  };

  const onChangeInput = (e) => {
    setUploaded(false);
    setError(false);
    setAlbumTitle(e.target.value);
  };

  const onSubmit = async () => {
    try {
      const { cover } = albumCover;
      const formData = new FormData();

      formData.append('cover', albumCover.cover);
      formData.append('title', albumTitle);
      photos.forEach(({ photo }) => {
        if (cover.name !== photo.name) formData.append('images', photo);
      });
      setLoading(true);
      const { fetchWithAuth } = apiService;
      const options = {
        method: 'POST',
        mode: 'same-origin',
        body: formData,
      };

      await fetchWithAuth('/uploads', options);
      setLoading(false);
      setUploaded(true);
      clearFields();
    } catch (error) {
      setLoading(false);
      setUploaded(false);
      setError(true);
      clearFields();
    }
  };

  return {
    cover: albumCover,
    photos: photos,
    albumTitle: albumTitle,
    loading,
    error,
    uploaded,
    onChangeCover,
    onChangePhotos,
    onSubmit,
    onChangeInput,
    isValidForm: isValidForm(),
  };
};
