import { useState, useEffect } from 'react';

export const useHttp = getData => {
  const [dataState, setDataState] = useState({
    data: null,
    loading: true,
    error: null,
  });

  useEffect(() => {
    let canceled = false;
    getData()
      .then(data => !canceled && setDataState({ data: data, loading: false }))
      .catch(
        error => !canceled && setDataState({ loading: false, error: error })
      );

    return () => (canceled = true);
  }, [getData]);

  return {
    loading: dataState.loading,
    data: dataState.data,
    error: dataState.error,
  };
};
