import {useState, useCallback, useEffect} from 'react'

const storageName = 'tokenData';

export const useMyContext = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const login = useCallback(() => {
        setIsAuthenticated(!! localStorage.getItem(storageName));
    }, [])

    const logout = useCallback(() => {
        localStorage.removeItem(storageName)
        setIsAuthenticated(!! localStorage.getItem(storageName));
    }, [])

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem(storageName));
        if (user && user.accessToken) {
            login(true)
        } else {
            login(false)
        }

    }, [login])


    return {login, logout, isAuthenticated}
}
