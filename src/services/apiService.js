

 class ApiService {
  _apiBaseUrl = '/api/';

  getResource = async (url) => {
    const res = await fetch(`${this._apiBaseUrl}${url}`);

    if (!res.ok) {
      throw new Error(`Could not fetch ${url}, received ${res.status}`);
    }

    return await res.json();
  };

  getAllAlbums = async () => {
    const res = await this.getResource('albums');
    return res;
  };

  getAllPhotos = async (id) => {
    const res = await this.getResource(`albums/${id}`);
    return res;
  };

  sendData = async (url, options) => {
    const res = await fetch(`${this._apiBaseUrl}${url}`, options);

    if (!res.ok) {
      throw new Error(`Could not fetch ${url}, received ${res.status}`);
    }

    return await res.json();
  };

  changePlaces = async (data, url ) =>{
    const options = {
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };
    const res = await this.sendData(url, options);
    return res;
  }

  deleteAlbum = async id =>{
    const options = {
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: id
      }),
    };
    const res = await this.sendData('auth/deleteAlbum', options);
    return res;
  }

  saveToken = (token) => {
    localStorage.setItem('tokenData', token);
  };
  //авторизация и получение первой пары токенов
  checkAuthAndGetToken = async ({ email, password }) => {
    ApiService.startTime = Date.now();
    const options = {
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    };
    let res;
    try{
       res = await this.sendData('auth/login', options);
    }
    catch (e) {
      return null;
    }
    this.saveToken(JSON.stringify(res));
    return res;
  };

  refreshToken = async (token) => {
    const options = {
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'content-type': 'application/json',
      },
      body: JSON.stringify({
        refreshToken: token,
      }),
    };
    const res = await this.sendData('auth/refreshToken', options);
    return res;
  };
  //для запросов в админке (с проверкой на аутентификацию)
  fetchWithAuth = async (url, options) => {
    let tokenData = null; //

    if (localStorage.getItem('tokenData')) {
      // если в localStorage присутствует tokenData, то берем её
      tokenData = JSON.parse(localStorage.tokenData);
    } else {
      throw new Error('auth failed');
    }

    if (!options.headers) {
      // если в запросе отсутствует headers, то задаем их
      options.headers = {};
    }

    if (tokenData) {
      if (Date.now() >= ApiService.startTime + Number(tokenData.expiresIn)) {
        // проверяем не истек ли срок жизни токена
        try {
          // если истек, то обновляем токен с помощью refreshToken
          const refresh = await this.refreshToken(tokenData.refreshToken);
          // сохраняем полученный обновленный токен в localStorage
          this.saveToken(JSON.stringify(refresh));
          tokenData = JSON.parse(localStorage.tokenData);
          ApiService.startTime = Date.now();
        } catch (e) {
          // если тут что-то пошло не так, то перенаправляем пользователя на страницу авторизации
          throw new Error('auth failed');
        }
      }
      options.headers.Authorization = `Bearer ${tokenData.accessToken}`; // добавляем токен в headers запроса
    }

    return await this.sendData(url, options); // вызываем изначальную функцию, но уже с валидным токеном в headers
  };
}

ApiService.startTime = 0;

export const apiService = new ApiService();