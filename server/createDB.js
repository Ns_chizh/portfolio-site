// здесь создаётся первичная база

const Files = require('./models/files');
const Albums = require('./models/albums');
const Photos = require('./models/photos');
const fs = require('fs');
const path = require('path');

const albumTitles = [
  'Ксюша и Олег',
  'Влада',
  'Омар',
  'Настя',
  'Таня',
  'Даша',
  'Аня',
  'студия "Верба"',
  'Пикник',
  'Андрей',
  'Дарья',
  'Лиза',
];

async function initPhotosCollection(filesCollection, albumsCollection) {
  try {
    const photos = filesCollection.map((el) => {
      const regExp = /album-([0-9]*)/;
      const matchNumber = el.path.match(regExp);
      if (matchNumber) {
        const albumName = albumTitles[Number(matchNumber[1]) - 1];
        const albumsReferens = albumsCollection.find(
          (el) => albumName === el.name
        );
        const path = el._id;
        const album = albumsReferens._id;
        return { path, album };
      }
      return el;
    });
    await Photos.create(photos);
  } catch (error) {
    console.error(error);
  }
}

async function initAlbumsCollection(files) {
  try {
    const covers = getCoversOfAlbums(files);
    const coversObj = covers.map((cover, i) => {
      return { name: albumTitles[i], cover: cover._id };
    });
    const albumsCollection = await Albums.create(coversObj);
    await initPhotosCollection(files, albumsCollection);
  } catch (error) {
    console.error(error);
  }
}

function getCoversOfAlbums(collection) {
  return collection
    .filter((img) => {
      const { path } = img;
      const idRegExp = /cover/g;
      if (idRegExp.test(path)) {
        return img;
      }
    })
    .sort((a, b) => {
      const regExp = /album-([0-9]*)/;
      const matchA = a.path.match(regExp);
      const matchB = b.path.match(regExp);

      return Number(matchA[1]) - Number(matchB[1]);
    });
}

async function initFilesCollection() {
  try {
    const paths = await getPathOfPhotos();
    const images = paths.map((item) => {
      return {
        path: item,
      };
    });
    const files = await Files.createFiles(images);
    await initAlbumsCollection(files);
  } catch (error) {
    console.error(error);
  }
}

function getPathOfPhotos() {
  const folderPath = '../../uploads';
  const files = fs.readdirSync(folderPath, (err, res) => {
    if (res) {
      return res;
    }
    throw new Error(err);
  });

  return files;
}

initFilesCollection();
