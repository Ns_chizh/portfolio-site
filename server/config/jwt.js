module.exports = {
    jwt:{
        secret: 'sUpERsecRetkEyoFJwT',
        tokens: {
            access: {
                type: 'access',
                expiresIn: '120000'
            },
            refresh: {
                type: 'refresh',
                expiresIn: '2592000000'
            },
        },
    },
};