const Photos = require('../models/photos');
const Files = require('../models/files');
const Albums = require('../models/albums');
const fs = require('fs');
const path = require('path')
exports.deleteAlbum = async function (req, res) {
    try {
        await Albums.removeAlbum(req.body.id);

        const photos = await Photos.getPhotos(req.body.id);
        const pathIDs = photos.map(photo => photo.path);
        const files = await Files.getFilesByID(pathIDs);
        const photosIds  = photos.map(photo => photo._id);
        const fileIds = files.map(file => file._id);

        await Files.removeFiles(fileIds);
        await Photos.removePhotos(photosIds);

        files.forEach(file => {
                fs.unlinkSync(path.resolve(__dirname, `../../../uploads/${file.path}`));
        })
        res.status(200).json({message: `Success`})
    } catch (e) {
        res.status(500).json({message: `The bad request`})
    }
}


