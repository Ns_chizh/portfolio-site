
const Files = require('../models/files');
const Albums = require('../models/albums');
const Photos = require('../models/photos');
const fs = require('fs');
const path = require('path');


exports.changePhotosPlaces = async function (req, res) {
    try {
        const paths = await Files.getFilesByID(req.body);
        let savedPathOne = paths[0].path;
        let savedPathTwo = paths[1].path;

        const firstPhotoId = await Photos.getIdByPath(paths[0]._id);
        const secondPhotoId = await Photos.getIdByPath(paths[1]._id);

        const firstCover = await Albums.getCoverById(firstPhotoId[0].path);
        const secondCover = await Albums.getCoverById(secondPhotoId[0].path);

        await fs.rename(path.resolve(__dirname, `../../../uploads/${savedPathOne}`), path.resolve(__dirname, `../../../uploads/tmp.jpg`), (error) => {
            if (error) throw error;
            console.log('renamed complete');
             fs.rename(path.resolve(__dirname, `../../../uploads/${savedPathTwo}`), path.resolve(__dirname, `../../../uploads/${savedPathOne}`), (error) => {
                if (error) throw error;
                console.log('renamed complete');
                 fs.rename(path.resolve(__dirname, `../../../uploads/tmp.jpg`), path.resolve(__dirname, `../../../uploads/${savedPathTwo}`), (error) => {
                    if (error) throw error;
                    console.log('renamed complete');
                });
            });
        });


        if(firstCover[0]){
            await Albums.updateCoverId(firstCover[0]._id, secondPhotoId[0].path);
        }else if(secondCover[0]){
            await Albums.updateCoverId(secondCover[0]._id, firstPhotoId[0].path);
        }

        return res.status(200).json({message: `Success`});

    } catch (e) {
        return res.status(500).json({message: `The bad request`});
    }
}
