const Files = require('../models/files');
const Albums = require('../models/albums');
const Photos = require('../models/photos');
const fs = require('fs')
const path = require('path');
async function uploadImages(req, res) {
  try {
    const { files, body } = req;
    const { cover } = files;
    const titleOfAlbum = body.title;

 /*   if(cover[0].path.split('uploads\\')[1] ){
      cover[0].path = cover[0].path.split('uploads\\')[1];
    }/!*    if(cover[0].path.split('uploads/')[1] ){
      cover[0].path = cover[0].path.split('uploads/')[1];
    }*!/
    let changeCoverName = cover[0].path.split('.');
    await fs.rename(path.resolve(__dirname, `../../../uploads/${cover[0].path}`), path.resolve(__dirname, `../../../uploads/${changeCoverName[0]}_cover.${changeCoverName[1]}`), (error) => {
      if (error) throw error;
    })
    cover[0].path =`${changeCoverName[0]}_cover.${changeCoverName[1]}`;*/
    const album = {
      name: titleOfAlbum,
    };
    //const createdPathCover = await Files.createOneFile(cover[0].path.split('uploads\\')[1]);
    const createdPathCover = await Files.createOneFile(cover[0].path.split('uploads/')[1]);
    album.cover = createdPathCover._id;
    const createdAlbum = await Albums.create(album);
    const paths = files.images.map(({ path }) => {
      return {
        //path: path.split('uploads\\')[1],
        path: path.split('uploads/')[1],
      };
    });
    const createdPathsOfImages = await Files.createFiles(paths);
    const images = createdPathsOfImages.map(({ _id }) => {
      return {
        path: _id,
        album: createdAlbum._id,
      };
    });

    const createdPhotosList = await Photos.createPhotosList([
      ...images,
      { path: createdPathCover._id, album: createdAlbum._id },
    ]);
    return res.json({ message: `Files have been uploaded.` });
  } catch (error) {
    res.status(500).json({ message: 'The bad request' });
  }
}
module.exports = uploadImages;
