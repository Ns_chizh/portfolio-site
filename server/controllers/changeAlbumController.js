const Albums = require('../models/albums');
const Photos = require('../models/photos');

exports.changeAlbumsPlaces = async function (req, res) {
    try{
        const ids = req.body;
        const firstAlbum = await Albums.getOneAlbum(ids[0]);
        const secondAlbum = await Albums.getOneAlbum(ids[1]);

        await Albums.updateName(firstAlbum._id, secondAlbum.name);
        await Albums.updateCover(firstAlbum._id, secondAlbum.cover);
        await Albums.updateName(secondAlbum._id, firstAlbum.name);
        await Albums.updateCover(secondAlbum._id, firstAlbum.cover);

        let photos = await Photos.getPhotos(firstAlbum._id);
        let photosIds = photos.map((photo) => photo._id);
        await Photos.updateAlbum(photosIds, 'tmp');

        photos = await Photos.getPhotos(secondAlbum._id);
        photosIds = photos.map((photo) => photo._id);
        await Photos.updateAlbum(photosIds, firstAlbum._id);

        photos =  await Photos.getPhotos('tmp');
        photosIds = photos.map((photo) => photo._id);
        await Photos.updateAlbum(photosIds, secondAlbum._id);

        return res.status(200).json({message: `${firstAlbum.name}`});
    }catch (e) {
        return res.status(500).json({message: `The bad request`});
    }
}