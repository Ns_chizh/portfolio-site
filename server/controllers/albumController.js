const Files = require('../models/files');
const Albums = require('../models/albums');
const Photos = require('../models/photos');

function findElement(collection, currentID) {
  const result = collection.find((el) => {
    if (el._id === currentID) {
      return el;
    }
  });
  return result;
}

exports.albumList = async (req, res) => {
  try {
    const albums = await Albums.getList();
    const filesID = albums.map((album) => album.cover);
    const files = await Files.getFilesByID(filesID);

    const body = albums.map((album) => {
      const { _id, cover } = album;
      const pathOfCover = findElement(files, cover);
      return {
        _id,
        cover: pathOfCover.path,
      };
    });

    res.status(200).json(body);
  } catch (error) {
    res.status(500).json({ message: 'The bad request' });
  }
};

exports.albumDetail = async (req, res) => {
  try {
    const { albumID } = req.params;
    const album = await Albums.getOneAlbum(albumID);

    if (!album) {
      return res.status(404).json({ message: `Album '${albumID}' not found` });
    }
    const photos = await Photos.getPhotos(album._id);
    const pathIDs = photos.map((photo) => photo.path);
    const files = await Files.getFilesByID(pathIDs);

    const collectionOfPhotos = photos.map((photo, i) => {
      const { path } = photo;
      const pathOfPhoto = findElement(files, path);
      return ({id:pathIDs[i], path: pathOfPhoto.path});
    });

    const body = {
      _id: album._id,
      name: album.name,
      photos: collectionOfPhotos,
    };

    res.status(200).json(body);
  } catch (error) {
    res.status(500).json({ message: 'The bad request' });
  }
};
