const Admins = require('../models/admins');
const Tokens = require('../models/tokens');
const authHelper = require('../helpers/authHelper');

const jwt = require('jsonwebtoken');
const {secret, tokens} = require('../config/jwt').jwt;
const bcrypt = require('bcryptjs');

function updateTokens(adminId) {
    const accessToken = authHelper.generateAccessToken(adminId);
    const refreshToken = authHelper.generateRefreshToken();

    return authHelper
        .replaceDbRefreshToken(refreshToken.id, adminId)
        .then(() => ({
            accessToken,
            refreshToken: refreshToken.token,
            expiresIn: tokens.access.expiresIn,
        }));
}

exports.checkAuth = async function (req, res) {
    try {
        const {email, password} = req.body;


        const admin = await Admins.findAdmin(email);
        if (!admin) {
            return res.status(401).json({message: 'Wrong data'});
        }

        const isValid = await bcrypt.compare(password, admin.password);
        if (isValid) {
            updateTokens(admin._id).then((tokens) => res.json(tokens));
        } else {
            return res.status(403).json({message: 'Access denied'});
        }
    } catch (e) {
        return res.status(500).json({message: `The bad request`});
    }
};

exports.refreshToken = async function (req, res) {
    const {refreshToken} = req.body;
    let payload;
    try {
        payload = jwt.verify(refreshToken, secret);
        if (payload.type !== 'refresh') {
            res.status(400).json({message: 'Invalid token'});
            return;
        }
    } catch (e) {
        if (e instanceof jwt.TokenExpiredError) {
            res.status(400).json({message: 'Token expired'});
            return;
        } else if (e instanceof jwt.JsonWebTokenError) {
            res.status(400).json({message: 'Invalid token'});
            return;
        }
    }

    const token = await Tokens.findToken(payload.id);
    if (token === null) {
        throw new Error('Invalid token');
    }

    const tokens = await updateTokens(token.adminId);
    try {
        res.status(200).json(tokens);
    } catch (err) {
        return res.status(400).json({message: err.message()})
    }
    ;
};
