const express = require('express');
const {Router} = require('express');
const router = Router();
const deleteController = require('../controllers/deleteController');


//  /api/auth
router.post('/deleteAlbum', deleteController.deleteAlbum);


module.exports = router;