const express = require('express');
const {Router} = require('express');
const router = Router();
const authController = require('../controllers/authController');
const changePhotosController = require('../controllers/changePhotoController');
const changeAlbumsController = require('../controllers/changeAlbumController');


//  /api/auth
router.post('/login', authController.checkAuth);
router.post('/refreshToken', authController.refreshToken);
router.post('/changePhotosPlaces', changePhotosController.changePhotosPlaces);
router.post('/changeAlbumsPlaces', changeAlbumsController.changeAlbumsPlaces);

module.exports = router;