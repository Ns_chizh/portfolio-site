const { Router } = require('express');
const router = Router();
const upload = require('../middleware/upload-middleware');
const authMiddleWare = require('../middleware/auth');
const uploadImages = require('../controllers/uploadController');

router.post('/uploads',authMiddleWare, upload);
router.post('/uploads', uploadImages);

module.exports = router;
