const { Router } = require('express');
const router = Router();
const albumController = require('../controllers/albumController');

router
  .get('/albums', albumController.albumList)
  .get('/albums/:albumID', albumController.albumDetail);
module.exports = router;
