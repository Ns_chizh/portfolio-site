const express = require('express');
const app = express();
const indexRouter = require('./routes/apiRoutes');
const uploadRouter = require('./routes/upload-images-routs');
const path = require('path');
const authRouter = require('./routes/authRoutes');
const deleteRouter = require('./routes/deleteImagesRoutes');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', indexRouter);
app.use('/api', uploadRouter);
app.use('/api/auth', authRouter);
app.use('/api/auth', deleteRouter);

app.use('/',express.static(path.join(__dirname, '../build')));
app.use('/uploads',express.static(path.join(__dirname, '../../uploads')));

app.get('/*', function(req, res){
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

//app.listen(3001, () => console.log('App has been started on port 3001'));
app.listen(80, () => console.log('App has been started on port 80'));
module.exports = app;
