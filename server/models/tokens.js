const Datastore = require('nedb');
const path = require('path');

const db = new Datastore({filename: path.resolve(__dirname,'../../../db/tokens') });
db.loadDatabase();


class Tokens {
    create({tokenId, adminId}) {
        db.insert({tokenId, adminId});
    }

    static async findToken(tokenId) {
        return new Promise((resolve, reject) => {
            db.findOne({tokenId: tokenId}, (err, token) => {
                if (err) reject(err);
                resolve(token);
            });
        });
    }

    static async removeToken(id) {
        return new Promise((resolve, reject) => {
            db.remove({adminId: id}, (err,numRemoved) => {
                if (err) reject(err);
                resolve(numRemoved);
            });
        });
    }

}

module.exports = Tokens;