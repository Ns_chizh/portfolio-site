const path = require('path');
const Datastore = require('nedb');

const db = new Datastore({ filename: path.resolve(__dirname,'../../../db/files')});
db.loadDatabase();

class Files {
  constructor(id) {
    this.fileID = id;
  }

  static async createOneFile(path) {
    return new Promise((resolve, reject) => {
      db.insert({ path: path }, (err, file) => {
        if (err) reject(err);
        resolve(file);
      });
    });
  }

  static async createFiles(paths) {
    return new Promise((resolve, reject) => {
      db.insert(paths, (err, file) => {
        if (err) reject(err);
        resolve(file);
      });
    });
  }

  static async getFileByID(ids) {
    return new Promise((resolve, reject) => {
      db.find({ _id:  ids }, (err, file) => {
        if (err) reject(err);
        resolve(file);
      });
    });
  }

  static async getFilesByID(ids) {
    return new Promise((resolve, reject) => {
      db.find({ _id: { $in: ids } }, (err, files) => {
        if (err) reject(err);
        resolve(files);
      });
    });
  }


  static async removeFiles(ids) {
    return new Promise((resolve, reject) => {
      db.remove({_id: { $in: ids }}, {multi: true},(err,numRemoved) => {
        if (err) reject(err);
        resolve(numRemoved);
      });
    });
  }
}

module.exports = Files;
