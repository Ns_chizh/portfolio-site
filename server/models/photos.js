const Datastore = require('nedb');
const path = require('path');

const db = new Datastore({ filename: path.resolve(__dirname,'../../../db/photos') });
db.loadDatabase();

class Photos {
  constructor() {}

  static create(photos) {
    return new Promise((resolve, reject) => {
      db.insert(photos, (err, albums) => {
        if (err) reject(err);
        resolve(albums);
      });
    });
  }

  static async createPhotosList(photos) {
    return new Promise((resolve, reject) => {
      db.insert(photos, (err, file) => {
        if (err) reject(err);
        resolve(file);
      });
    });
  }

  static async getPhotos(id) {
    return new Promise((resolve, reject) => {
      db.find({ album: `${id}` }, (err, photos) => {
        if (err) reject(err);
        resolve(photos);
      });
    });
  }

  static async getIdByPath(id) {
    return new Promise((resolve, reject) => {
      db.find({ path: `${id}` }, (err, photos) => {
        if (err) reject(err);
        resolve(photos);
      });
    });
  }
  static async removePhotos(ids) {
    return new Promise((resolve, reject) => {
      db.remove({_id: { $in: ids }},  {multi: true},(err,numRemoved) => {
        if (err) reject(err);
        resolve(numRemoved);
      });
    });
  }
  static async updateAlbum(ids, newId) {
    return new Promise((resolve, reject) => {
      db.update({_id: { $in: ids }},{$set: {album:newId}}, {multi: true} ,(err) => {
        if (err) reject(err);
        resolve();
      });
    });
  }
}

module.exports = Photos;
