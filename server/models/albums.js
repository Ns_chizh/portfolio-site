const Datastore = require('nedb');
const path = require('path');

const db = new Datastore({ filename: path.resolve(__dirname,'../../../db/albums') });
db.loadDatabase();

class Albums {
  static async getOneAlbum(id) {
    return new Promise((resolve, reject) => {
      db.findOne({ _id: id }, (err, albums) => {
        if (err) reject(err);
        resolve(albums);
      });
    });
  }

  static create(covers) {
    return new Promise((resolve, reject) => {
      db.insert(covers, (err, albums) => {
        if (err) reject(err);
        resolve(albums);
      });
    });
  }

  static async getList() {
    return new Promise((resolve, reject) => {
      db.find({}, (err, albums) => {
        if (err) reject(err);
        resolve(albums);
      });
    });
  }

  static async removeAlbum(id) {
    return new Promise((resolve, reject) => {
      db.remove({_id: id}, (err,numRemoved) => {
        if (err) reject(err);
        resolve(numRemoved);
      });
    });
  }

  static async getCoverById(id) {
    return new Promise((resolve, reject) => {
      db.find({cover: id}, (err,album) => {
        if (err) reject(err);
        resolve(album);
      });
    });
  }

  static async updateCoverId(albumId, coverId) {
    return new Promise((resolve, reject) => {
      db.update({_id: albumId},{$set: {cover:coverId}}, (err) => {
        if (err) reject(err);
        resolve();
      });
    });
  }


  static async updateCover(id, newCover) {
    return new Promise((resolve, reject) => {
      db.update({_id: id},{$set: {cover:newCover}}, (err) => {
        if (err) reject(err);
        resolve();
      });
    });
  }
  static async updateName(id, newName) {
    return new Promise((resolve, reject) => {
      db.update({_id: id},{$set: {name:newName}}, (err) => {
        if (err) reject(err);
        resolve();
      });
    });
  }
}

module.exports = Albums;
