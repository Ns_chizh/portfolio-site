const Datastore = require('nedb');
const path = require('path');

const db = new Datastore({ filename: path.resolve(__dirname,'../../../db/admins') });
db.loadDatabase();


class Admins {
    create({ email, password }) {
        db.insert({ email, password });
    }
    static async findAdmin(email) {
        return new Promise((resolve, reject) => {
            db.findOne({ email: email }, (err, admin) => {
                if (err) reject(err);
                resolve(admin);
            });
        });
    }

}

module.exports = Admins;

