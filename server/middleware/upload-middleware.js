const multer = require('multer');
const path = require('path');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(__dirname, '../../../uploads'));
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const uploadFile = multer({
  storage: storage,
  limits: { fileSize: 100000000 },
}).fields([
  { name: 'images', maxCount: 12 },
  { name: 'cover', maxCount: 1 },
  { name: 'title', maxCount: 1 },
]);

module.exports = uploadFile;
