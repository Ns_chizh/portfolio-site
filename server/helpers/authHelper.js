const uuid = require('uuid/v4');
const jwt = require('jsonwebtoken');
const {secret, tokens} = require('../config/jwt').jwt;
const Tokens = require('../models/tokens');


const generateAccessToken = (adminId) =>{
    const payload = {
        adminId,
        type: tokens.access.type
    };
    const options = { expiresIn: tokens.access.expiresIn};

    return jwt.sign( payload, secret, options);
};

const generateRefreshToken = () =>{
    const payload = {
        id: uuid(),
        type: tokens.refresh.type
    };
    const options = { expiresIn: tokens.refresh.expiresIn};

    return {
        id: payload.id,
        token: jwt.sign(payload,secret,options)
    }
};

//перезаписывание refresh токена в базе
const replaceDbRefreshToken = async (tokenId, adminId) => {
    await Tokens.removeToken(adminId);
    const token = new Tokens();
    token.create({tokenId, adminId});
};

module.exports = {
    generateAccessToken,
    generateRefreshToken,
    replaceDbRefreshToken
}